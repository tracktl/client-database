
//import Party   from './models/party.js'
import Mysql   from 'mysql'
import options from './options.js'

export default class DataBaseCli {

    constructor( im, options ) {

        this.im = im

    }

    setIM( im ) {

        this.im = im

        return this

    }

    getUsers( partyID ) {
        if ( !partyID ) return false

        return this.im.request( 'database:party:users', { partyID : partyID } )
            .catch( (...err) => { throw new Error('Error getUsers:', err) } )
    }

    getUsersByToken( tokenMailing ) {
        if ( !tokenMailing ) return false

        return this.im.request( 'database:party:users', { tokenMailing : tokenMailing } )
            .catch( (...err) => { throw new Error('Error getUsers:', err) } )
    }


    getParty( partyID ) {
        if ( !partyID ) return false

        return this.im.request( 'database:party:get', { partyID : id } )
            .catch( (...err) => { throw new Error('Error getUsers:', err) } )
    }

}

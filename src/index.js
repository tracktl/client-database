
import options from './options'
import IM from 'internal-messaging'
import DataBaseCli from './database.js'

module.exports = ( imUser ) => {

    var dataBase = new DataBaseCli()

    return new Promise( (resolve, fail) => {

        // L'utilisateur envoi un im valide
        if (imUser && imUser instanceof IM && imUser._conn) {

            resolve( dataBase.setIM( imUser ) )

        } else {

            const im = new IM()

            im.connect(options.im)
            .then(() => {
                console.log('[Success] Connect ready')

                resolve( dataBase.setIM( im ) )

            })
            .catch((...err) => {
                console.log('[Err]', err)

                fail( err )
            })
        }

    } )


}

import tape from 'tape'

import myModule from '../src/index.js'


tape('Test module', function (t) {
    t.plan(2)

    const module = myModule()

    module.then( (db) => {
        t.equal( typeof db, 'object', 'isObject')

        db.getUsers( 34781 ).then( ( users ) => {

            t.equal( typeof users, 'object', 'isArray ' + users.length)

        } )

    } )
    .catch( ( ...err ) => {
        console.log('Error', err)
    } )


});

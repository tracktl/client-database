# Client DataBase

```javascript
import DataBase from 'client-database'
const bdd = new DataBase({ direct : true })
```

## Options

```js
{
    direct:  true, // Ouvrir une connexion direct a la bdd ( n'utilise pas rabbit )
    timeout: 0,    // Temps avant l'ouverture d'une co direct
    mysql : {
        // Toutes les options mysql.
    }
}
```

## Basic query

```javascript
const req = dataBase.query('SELECT * FROM ...')

const req = dataBase.execute('SELECT * FROM ... WHERE id = ?', [id, ...])

const req = dataBase.execute('SELECT * FROM ... WHERE id = :id', { id : ''})

req.then((rows) => {

}).catch((err) => {
    throw new Error(err)
})
```

## Default models


* ### Party

```javascript
// Party infos
const req = dataBase.party( id ).get()

// Get all songs on party
const req = dataBase.party( id ).getSongs()

// Get all users on party
const req = dataBase.party( id ).getUsers()

req.then((rows) => {

}).catch((err) => {
    throw new Error(err)
})
```

* ### Song & Track

```javascript
dataBase.getSong( id )
dataBase.getTrack( id )
```

* ### Users

```javascript
dataBase.getUser({ userID : id })
```

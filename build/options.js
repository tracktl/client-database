'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {

    /* IM */
    im: {
        host: process.env.IM_HOST || '192.168.99.100',
        port: process.env.IM_PORT || '5672'
    }

};
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
//import Party   from './models/party.js'


var _mysql = require('mysql');

var _mysql2 = _interopRequireDefault(_mysql);

var _options = require('./options.js');

var _options2 = _interopRequireDefault(_options);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DataBaseCli = function () {
    function DataBaseCli(im, options) {
        _classCallCheck(this, DataBaseCli);

        this.im = im;
    }

    _createClass(DataBaseCli, [{
        key: 'setIM',
        value: function setIM(im) {

            this.im = im;

            return this;
        }
    }, {
        key: 'getUsers',
        value: function getUsers(partyID) {
            if (!partyID) return false;

            return this.im.request('database:party:users', { partyID: partyID }).catch(function () {
                for (var _len = arguments.length, err = Array(_len), _key = 0; _key < _len; _key++) {
                    err[_key] = arguments[_key];
                }

                throw new Error('Error getUsers:', err);
            });
        }
    }, {
        key: 'getUsersByToken',
        value: function getUsersByToken(tokenMailing) {
            if (!tokenMailing) return false;

            return this.im.request('database:party:users', { tokenMailing: tokenMailing }).catch(function () {
                for (var _len2 = arguments.length, err = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                    err[_key2] = arguments[_key2];
                }

                throw new Error('Error getUsers:', err);
            });
        }
    }, {
        key: 'getParty',
        value: function getParty(partyID) {
            if (!partyID) return false;

            return this.im.request('database:party:get', { partyID: id }).catch(function () {
                for (var _len3 = arguments.length, err = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
                    err[_key3] = arguments[_key3];
                }

                throw new Error('Error getUsers:', err);
            });
        }
    }]);

    return DataBaseCli;
}();

exports.default = DataBaseCli;
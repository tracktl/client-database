'use strict';

var _options = require('./options');

var _options2 = _interopRequireDefault(_options);

var _internalMessaging = require('internal-messaging');

var _internalMessaging2 = _interopRequireDefault(_internalMessaging);

var _database = require('./database.js');

var _database2 = _interopRequireDefault(_database);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (imUser) {

    var dataBase = new _database2.default();

    return new Promise(function (resolve, fail) {

        // L'utilisateur envoi un im valide
        if (imUser && imUser instanceof _internalMessaging2.default && imUser._conn) {

            resolve(dataBase.setIM(imUser));
        } else {
            (function () {

                var im = new _internalMessaging2.default();

                im.connect(_options2.default.im).then(function () {
                    console.log('[Success] Connect ready');

                    resolve(dataBase.setIM(im));
                }).catch(function () {
                    for (var _len = arguments.length, err = Array(_len), _key = 0; _key < _len; _key++) {
                        err[_key] = arguments[_key];
                    }

                    console.log('[Err]', err);

                    fail(err);
                });
            })();
        }
    });
};